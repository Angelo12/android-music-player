package com.mdf3.angelogomez.a_gomez_labfour;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity implements ControlsFragment.PlayerControlsInterface, ServiceConnection {

    AudioService mService;
    boolean isBound = false;
    BroadcastReceiver mReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getFragmentManager().beginTransaction().replace(R.id.main_bot_container, ControlsFragment.newInstance("", ""), ControlsFragment.TAG).commit();


        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.getAction() == "Update.MediaPlayer") {
                    Song song = (Song) intent.getSerializableExtra("theSong");
                    PlayerFragment frag = PlayerFragment.newInstance(song);
                    getFragmentManager().beginTransaction().replace(R.id.main_top_container, frag, PlayerFragment.TAG).commit();
                }

                if(intent.getAction() == AudioService.ACTION_SKIP_B) {
                    iSkipBackward();
                }

                if(intent.getAction() == AudioService.ACTION_SKIP_F) {
                    iSkipForward();
                }

                if(intent.getAction() == AudioService.ACTION_STOP) {
                    iStop();
                }


            }
        };


    }

    @Override
    protected void onResume() {
        super.onResume();
        isBound = false;

        IntentFilter intentFilter = new IntentFilter("Update.MediaPlayer");
        registerReceiver(mReceiver, intentFilter);

        Intent audioService = new Intent(this, AudioService.class);
        bindService(audioService, this, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(this);
    }

    @Override
    public void iSkipBackward() {
        if (isBound && mService != null) {
            mService.skipBackward();
        }
    }

    @Override
    public void iSkipForward() {
        if (isBound && mService != null) {
            mService.skipForward();
        }
    }

    @Override
    public void iPlay() {
        if (isBound && mService != null) {
            mService.startPlayback();
        }
    }

    @Override
    public void iStop() {
        if (isBound && mService != null) {
            mService.stopPlayback();
        }
    }

    @Override
    public void iPause() {
        if (isBound && mService != null) {
            mService.pausePlayback();
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        AudioService.AudioServiceBinder binder = (AudioService.AudioServiceBinder) service;
        mService = binder.getService();
        isBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mService = null;
        isBound = false;
    }
}
