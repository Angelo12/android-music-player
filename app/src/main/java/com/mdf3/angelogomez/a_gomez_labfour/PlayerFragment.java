package com.mdf3.angelogomez.a_gomez_labfour;


import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PlayerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlayerFragment extends Fragment {

    public static final String TAG = "PlayerFragment.TAG";
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private Song mSong;


    public static PlayerFragment newInstance(Song _song) {
        PlayerFragment fragment = new PlayerFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, _song);
        fragment.setArguments(args);
        return fragment;
    }

    public PlayerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSong = (Song) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_player, container, false);

        TextView tvArtist = (TextView) v.findViewById(R.id.artist_name);
        TextView tvAlbumName = (TextView) v.findViewById(R.id.album_name);
        TextView tvSongName = (TextView) v.findViewById(R.id.track_name);
        ImageView ivAlbum = (ImageView) v.findViewById(R.id.album_art);

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(getActivity(), mSong.getParsedUri());

        tvArtist.setText(mSong.mArtist);
        tvAlbumName.setText(mSong.mAlbumName);
        tvSongName.setText(mSong.mTitle);
        byte[] albumArt = mmr.getEmbeddedPicture();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        ivAlbum.setImageBitmap(BitmapFactory.decodeByteArray(albumArt, 0, albumArt.length, options));

        // Inflate the layout for this fragment
        return v;
    }


}
