package com.mdf3.angelogomez.a_gomez_labfour;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.RemoteController;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

public class AudioService extends Service implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {


    public static final String ACTION_STOP = "ACTION_STOP";
    public static final String ACTION_SKIP_F = "ACTION_SKIP_FORWARD";
    public static final String ACTION_SKIP_B = "ACTION_SKIP_BACKWARD";

    //Service Stuff
    public AudioService() {
    }

    public class AudioServiceBinder extends Binder {
        public AudioService getService() {
            return AudioService.this;
        }
    }


    private NotificationCompat.Action generateAction(int icon, String title, String intentAction) {
        Intent intent =  new Intent(getApplicationContext(), MainActivity.class);
        intent.setAction(intentAction);
        PendingIntent pIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
        return new NotificationCompat.Action.Builder(icon, title, pIntent).build();
    }

    private void makeNotification() {

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(getApplicationContext(), mSongList.get(mCurrentSong).getParsedUri());
        byte[] picture = mmr.getEmbeddedPicture();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        options.outHeight = 64;
        options.outWidth = 64;

       // PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0 , new Intent(getApplicationContext(), MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext());
        NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle(notification);
        //notification.setAutoCancel(false);
        notification.setOngoing(true)
                .setLargeIcon(BitmapFactory.decodeByteArray(picture, 0, picture.length, options))
                .setSmallIcon(R.drawable.ic_play)
                .setTicker("Music Playing")
                .setWhen(System.currentTimeMillis())
                .setContentTitle(getCurrentSong().mTitle)
                .setContentText(getCurrentSong().mArtist + " - " + getCurrentSong().mAlbumName);
      //          .setContentIntent(pi);

        style.bigPicture(BitmapFactory.decodeByteArray(picture, 0, picture.length, options));
        style.setSummaryText(getCurrentSong().mArtist + " - " + getCurrentSong().mAlbumName);
        notification.setStyle(style);


        notification.addAction(generateAction(R.drawable.ic_skip_backwards, "", ACTION_SKIP_B));
        notification.addAction(generateAction(R.drawable.ic_stop, "", ACTION_STOP));
        notification.addAction(generateAction(R.drawable.ic_skip_forward, "", ACTION_SKIP_F));


        startForeground(FOREGROUND_ID, notification.build());
        //nMgr.notify(NOTIFICATION_ID, notification.build());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        loadTracks();
       // makeNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) { return super.onStartCommand(intent, flags, startId); }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return new AudioServiceBinder();
    }

    //Media Player Stuff
    public MediaPlayer mPlayer;
    private ArrayList<Song> mSongList;
    private int mStatus;
    private int mCurrentSong = 0;

    private static final int MP_IDLE = 0;
    private static final int MP_INITIALIZED = 1;
    private static final int MP_PREPARED = 2;
    private static final int MP_STARTED = 3;
    private static final int MP_PAUSED = 4;
    private static final int MP_STOPPED = 5;
    private static final int MP_PLAYBACK_COMPLETED = 6;
    private static final int MP_ERROR = 7;

    private static final int FOREGROUND_ID = 0x010101;
    private static final int NOTIFICATION_ID = 0x010100;

    private void printStatus() {

        String log = "MediaPlayer State: ";
        String fullLog;

        switch (mStatus) {
            case MP_IDLE:
                fullLog = log + "IDLE";
                break;
            case MP_INITIALIZED:
                fullLog = log + "INITIALIZED";
                break;
            case MP_PREPARED:
                fullLog = log + "PREPARED";
                break;
            case MP_STARTED:
                fullLog = log + "STARTED";
                break;
            case MP_PAUSED:
                fullLog = log + "PAUSED";
                break;
            case MP_STOPPED:
                fullLog = log + "STOPPED";
                break;
            case MP_PLAYBACK_COMPLETED:
                fullLog = log + "PLAYBACK_COMPLETED";
                break;
            default:
                return;
        }

        Log.i("AudioService", fullLog);
    }

    public void loadTracks() {
        mSongList = new ArrayList<>();

        String song1Uri = "android.resource://" + getPackageName() + "/" + R.raw.song_1;
        String song2Uri = "android.resource://" + getPackageName() + "/" + R.raw.song2;
        String song3Uri = "android.resource://" + getPackageName() + "/" + R.raw.song3;

        mSongList.add(makeSong(song1Uri));
        mSongList.add(makeSong(song2Uri));
        mSongList.add(makeSong(song3Uri));
    }



    public Song makeSong(String uri) {
        Song x;

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(getApplicationContext(), Uri.parse(uri));

        String artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        String albumName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);

        x = new Song(title, artist, albumName, uri);
        return x;
    }

    public Song getCurrentSong() {
        return mSongList.get(mCurrentSong);
    }

    public void skipForward() {
        if(mPlayer != null) {
            mCurrentSong++;
            if(mCurrentSong >= mSongList.size()) {
                mCurrentSong = 0;
            }
            playSong();
        }
        printStatus();
    }

    public void skipBackward() {
        if(mPlayer != null) {
            mCurrentSong--;
            if(mCurrentSong < 0) {
                mCurrentSong = mSongList.size() - 1;
            }
            playSong();
        }
        printStatus();
    }

    public void pausePlayback() {
        if(mStatus == MP_STARTED || mStatus == MP_PAUSED || mStatus == MP_PLAYBACK_COMPLETED) {
            mPlayer.pause();
            mStatus = MP_PAUSED;
        }
        printStatus();
    }

    public void stopPlayback() {
        if(mStatus == MP_STARTED || mStatus == MP_PAUSED || mStatus == MP_PLAYBACK_COMPLETED) {
            mPlayer.stop();
            mStatus = MP_STOPPED;
        }
        printStatus();
    }

    public void startPlayback() {

        if(mStatus == MP_PAUSED) {
            mPlayer.start();
            mStatus = MP_STARTED;
        } else if(mStatus == MP_STARTED) {

        } else {
            playSong();
        }
        printStatus();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.i("Last Known State", "State: " + mStatus);
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mStatus = MP_PLAYBACK_COMPLETED;

        if(!mPlayer.isLooping()) {
            mCurrentSong++;
        }

        if(mCurrentSong >= mSongList.size()) {
            mCurrentSong = 0;
        }

        playSong();
        printStatus();
    }


    @Override
    public void onPrepared(MediaPlayer mp) {
        mStatus = MP_PREPARED;
        mPlayer.start();
        mStatus = MP_STARTED;

        Intent intent = new Intent("Update.MediaPlayer");
        intent.putExtra("theSong", getCurrentSong());
        sendBroadcast(intent);

        makeNotification();
        printStatus();
    }


    public void setLooping(boolean _loop) {
        if(mPlayer != null && mStatus != MP_ERROR) {
            mPlayer.setLooping(_loop);
        }
    }

    public void playSong() {
        if(mPlayer != null){
            mPlayer.release();
        }

        mPlayer = new MediaPlayer();
        mStatus = MP_IDLE;
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);

        try {
            mPlayer.setDataSource(getApplicationContext(), mSongList.get(mCurrentSong).getParsedUri());
            mStatus = MP_INITIALIZED;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            mPlayer.prepareAsync();
            printStatus();
        }
    }
}
