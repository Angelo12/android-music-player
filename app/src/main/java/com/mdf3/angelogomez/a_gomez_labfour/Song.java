package com.mdf3.angelogomez.a_gomez_labfour;

import android.net.Uri;

import java.io.Serializable;

/**
 * Created by AngeloGomez on 10/7/15.
 */
public class Song implements Serializable {

    String mTitle;
    String mArtist;
    String mAlbumName;
    String mUri;

    public Song(String mTitle, String mArtist, String mAlbumName, String mUri) {
        this.mTitle = mTitle;
        this.mArtist = mArtist;
        this.mAlbumName = mAlbumName;
        this.mUri = mUri;
    }

    public Uri getParsedUri() {
        return Uri.parse(mUri);
    }

}
