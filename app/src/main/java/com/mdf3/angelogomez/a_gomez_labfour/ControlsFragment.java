package com.mdf3.angelogomez.a_gomez_labfour;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ControlsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ControlsFragment extends Fragment  implements  View.OnClickListener {



    public interface PlayerControlsInterface {
        public void iSkipBackward();
        public void iSkipForward();
        public void iPlay();
        public void iStop();
        public void iPause();
    }

    PlayerControlsInterface mDelegate;

    public static final String TAG = "ControlsFragment.TAG";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public static ControlsFragment newInstance(String param1, String param2) {
        ControlsFragment fragment = new ControlsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(activity instanceof PlayerControlsInterface) {
            mDelegate = (PlayerControlsInterface) activity;
        }
    }

    public ControlsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_controls, container, false);
        // Inflate the layout for this fragment

        v.findViewById(R.id.skip_backwards).setOnClickListener(this);
        v.findViewById(R.id.skip_forward).setOnClickListener(this);
        v.findViewById(R.id.play).setOnClickListener(this);
        v.findViewById(R.id.stop).setOnClickListener(this);
        v.findViewById(R.id.pause).setOnClickListener(this);
        return v;
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.skip_backwards) {
            mDelegate.iSkipBackward();
        } else if (v.getId() == R.id.skip_forward) {
            mDelegate.iSkipForward();
        } else if (v.getId() == R.id.play) {
            mDelegate.iPlay();
        } else if (v.getId() == R.id.stop) {
            mDelegate.iStop();
        } else if (v.getId() == R.id.pause) {
            mDelegate.iPause();
        }
    }
}
